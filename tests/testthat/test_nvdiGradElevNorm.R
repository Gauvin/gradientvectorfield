

test_that( "nvdi with elev gradient norm", {
  
  
  myneighRasterFactory <- neighRaster::neighRasterQcFactory$new(
    rasterPath =  file.path("/", "home","charles","Projects","Vegetation", "Data","GeoData","Raster", "cdem_dem_021L_croppedQc.tif" ),
    neighPath = file.path("/", "home","charles","Projects","Vegetation", "Data","GeoData","QcNeighbourhoods")
  )
  
  myneighShpFactory <- neighShp::neighShpQcFactory$new(neighPath = file.path("/", "home","charles","Projects","Vegetation", "Data","GeoData","QcNeighbourhoods"))
  
  myRasterGradientList <- neighRasterGradientListByNeigh$new(neighRasterFactory =myneighRasterFactory,
                                                             neighShpFactory = myneighShpFactory)
  
  listNeigh <- c("Saint-Jean-Baptiste")
  listNeighRasterGradient <- list()
  for(n in listNeigh){
    listNeighRasterGradient <- rlist::list.append(listNeighRasterGradient,
                                                  myRasterGradientList$getRasterFixedNeigh( n) 
    )
  }
  
  
  
  
  
  
  
})



test_that( "nvdi with elev gradient norm", {
 
  
  listNeigh <-  c("Montcalm", "Saint-Jean-Baptiste","Vieux-Québec/Cap-Blanc/Colline parlementaire")
 
  
  neighRastQcNVDI <- neighRaster::neighRasterQc$new( rasterPath = file.path("/", "home","charles","Projects","Vegetation", "Data","GeoData","Raster", "nvdiQcNoCloudsCropped4326.tif" ) ,
                                                     neighPath =file.path("/", "home","charles","Projects","Vegetation", "Data","GeoData","QcNeighbourhoods"),
                                                     listNeigh = listNeigh,
                                                     useBbox = F)
  neighRastQcElev <- neighRaster::neighRasterQc$new( rasterPath = here::here("Data","GeoData","Raster", "cdem_dem_021L_croppedQc.tif" ),
                                                     neighPath =here::here("Data","GeoData","QcNeighbourhoods"),
                                                     listNeigh = listNeigh,
                                                     useBbox = F)
  rast <- neighRastQcNVDI$getRaster()
  
  rast %>% raster::plot()
  
  myRastGradient <- neighRasterGradient$new(neighRaster = neighRastQcElev,
                                            strFileName = "centralCroppedElev",
                                            rasterPathDirWrite = file.path("/", "home","charles","Projects","Vegetation", "Data","GeoData","Raster", "GradientNorm","Quebec")
  )

  myRastGradient$getRaster() %>% raster::plot()
})